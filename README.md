# Electrophysiology - TDT Stimulation and Recording

Repo of the code and files necessary to run a Tucker-Davis-Technologies based Hardware set-up to perform electrophysiological experiments with acoustic and electric stimulation.

# Required Hardware

*  TDT RX6 Processor
*  TDT RX5 Processor
*  TDT RA16AC Headstage
*  PA5 attenuators (x2)
*  ATT20 attenuator bank (8 channels)
*  8 channel current source
*  MF1 configurable speakers (x2)

More pieces of equipment will be necessary depending on your precise subject interface.

# Required External software
* TDT BrainWare
* Matlab (2012a or newer)

# To use:

* Clone repo to your projects directory
* Open BrainWare and Matlab. Turn Hardware on
* Connect to your RX5 and load RX5.rco. Activate desired channels
* Activate the Matlab GUI and vary the relevant parameters. A more complete explanation of GUI usage is in the subfolder
