function varargout = settings_2018(varargin)
% SETTINGS_2018 M-file for settings_2018.fig
%
% See also: GUIDE, GUIDATA, GUIHANDLES
% Edit the above text to modify the response to help settings_2018
% Last Modified by GUIDE v2.5 11-Dec-2018 13:34:15

% - - - - - - - - - - - CHANGELOG - - - - - - - - - - -
% Every change of this program has to be documented (when, who, what, why)
% 
% 14.11.2014, AW: Changed tab-order in GUI
%           Added setting of Presentation Order of StimGrid. 

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @settings_2018_OpeningFcn, ...
                   'gui_OutputFcn',  @settings_2018_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before settings_2018 is made visible.
function settings_2018_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to settings_2018 (see VARARGIN)

% Choose default command line output for settings_2018
handles.output = hObject;

set(handles.sweepLength, 'String', varargin{1}.sweepLength);
set(handles.repRate, 'String', varargin{1}.repRate);
set(handles.presOrder, 'String', varargin{1}.presOrder);
set(handles.stimRepeats, 'String', varargin{1}.stimRepeats);
set(handles.respStart, 'String', varargin{1}.respStart);
set(handles.respEnd, 'String', varargin{1}.respEnd);
set(handles.sponStart, 'String', varargin{1}.sponStart);
set(handles.sponEnd, 'String', varargin{1}.sponEnd);

handles.sweepLength = varargin{1}.sweepLength;
handles.repRate = varargin{1}.repRate;
handles.presOrder = varargin{1}.presOrder;
handles.stimRepeats = varargin{1}.stimRepeats;
handles.respStart = varargin{1}.respStart;
handles.respEnd = varargin{1}.respEnd;
handles.sponStart = varargin{1}.sponStart;
handles.sponEnd = varargin{1}.sponEnd;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes settings_2018 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = settings_2018_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pb_ok.
function pb_ok_Callback(hObject, eventdata, handles)
% hObject    handle to pb_ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

save settings_2018.mat;
close

function respStart_Callback(hObject, eventdata, handles)
% hObject    handle to respStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of respStart as text
%        str2double(get(hObject,'String')) returns contents of respStart as a double
    handles.respStart = str2double(get(hObject, 'String'));
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function respStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to respStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function respEnd_Callback(hObject, eventdata, handles)
% hObject    handle to respEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of respEnd as text
%        str2double(get(hObject,'String')) returns contents of respEnd as a double

    handles.respEnd = str2double(get(hObject, 'String'));
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function respEnd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to respEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function repRate_Callback(hObject, eventdata, handles)
% hObject    handle to repRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of repRate as text
%        str2double(get(hObject,'String')) returns contents of repRate as a double

    handles.repRate = str2double(get(hObject, 'String'));
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function repRate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to repRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function stimRepeats_Callback(hObject, eventdata, handles)
% hObject    handle to stimRepeats (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of stimRepeats as text
%        str2double(get(hObject,'String')) returns contents of stimRepeats as a double

    handles.stimRepeats = str2double(get(hObject, 'String'));
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function stimRepeats_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stimRepeats (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sweepLength_Callback(hObject, eventdata, handles)
% hObject    handle to sweepLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sweepLength as text
%        str2double(get(hObject,'String')) returns contents of sweepLength as a double

    handles.sweepLength = str2double(get(hObject, 'String'));
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function sweepLength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sweepLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sponStart_Callback(hObject, eventdata, handles)
% hObject    handle to sponStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sponStart as text
%        str2double(get(hObject,'String')) returns contents of sponStart as a double

    handles.SponStart = str2double(get(hObject, 'String'));
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function sponStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sponStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function sponEnd_Callback(hObject, eventdata, handles)
% hObject    handle to sponEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sponEnd as text
%        str2double(get(hObject,'String')) returns contents of sponEnd as a double

    handles.sponEnd = str2double(get(hObject, 'String'));
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function sponEnd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sponEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function presOrder_Callback(hObject, eventdata, handles)
% hObject    handle to presOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of presOrder as text
%        str2double(get(hObject,'String')) returns contents of presOrder as a double


% --- Executes during object creation, after setting all properties.
function presOrder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to presOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
