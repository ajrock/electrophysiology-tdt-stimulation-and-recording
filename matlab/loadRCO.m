function RX8 = loadRCO(handles)

assert(isfield(handles,'rco'),'Invalid RCO file!')

RX8=actxcontrol('RPco.x',[0 0 0 0]);
if(~invoke(RX8,'ConnectRX8','GB',1))
    error('Cannot open connect RX8');
end

invoke(RX8,'ClearCOF');
if(~invoke(RX8,'LoadCOF',handles.rco))
    error('Cannot load RX8 circuit');
end