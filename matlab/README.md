# AcouElecStim 

## Goal
This GUI is used to control the creation of a Stimulus Grid in Brainware that will vary the necessary parameters of an E'Phys experiment. BrainWare will randomly select rows from the StimGrid created from the displayed parameters, and feed them through the relevant RCO files loaded on the hardware in order to record and stimulate responses.

![Main Gui](AcouElecStimGUI.jpg)

## Quick Usage
*  Stimulus Modii, Acoustic Stim Type, and Mon/Binaural must all be set before the program can successfully download the StimGrid
*  In general, the main GUI controls stimulation parameters, while Sweep Settings controls recording and repetition parameters
*  Changing Presets will immediately format the parameters to a valid StimGrid

## A longer explanation
The program will create combinations for each stimulus mode that indicates a list of values. For example. One can stimulate at 70, 75, and 80 dB while also testing ITDs between -2000 and 2000. 
One may seperate the GUI functions into types: 
* Stimulus Types. The group Stim Config largely handles this. This determines the overall shape of the stimulus to be created, be it click, chirp, pulse, noise, or tone. These can be further subdivided into stimulus parameters, where each stimulus type has various parameters that may be changed depending on context of the experiment.
* Train/mux parameters. Many experiments call for repetition of stimuli of particular mixes of acoustic and electric stimulation, as well as calibrating responses, to aid in later examination.
* Experiment type. Left-most five panels. Determine the primary variables to vary over the experiment.
* Recording parameters. Found under Sweep Settings. Determines the length of recorded time, as well as the pause between stimulations, number of repeitions of stimulation to give, and how to present stimuli

## Experiment Parameters
### Stim Levels (IO)
Allows one to set the stimulation levels used, both acoustically and electrically. Can be set regardless of whether the stimulation mode (acou or elec) is actually used. Highly recommended that one calibrates both signals via external microscope and/or oscilloscope. 
For acoustic stimulation, creates a list of stimulation steps, starting at Min dB SPL and increasing by step size until the level is higher than (non-inclusive) or equal to (inclusive)  Max dB SPL. This operates by assuming that 0 dB of attenuation outputs a 100 dB SPL signal, and adjusting attenuation values to create the desired level.
For electric stimulation, behaves similarly, except all stimulus levels are negative, relative to 0 dB attenuation. That means e.g. 20 dB Att outputs a smaller signal than 0 att. Otherwise, steps are created in the same fashion as acoustic. In the Vollmer Lab at LIN, this is calibrated to 0.1 mA output peak2peak at 36 dB attenuation.
### Sine/Tone
Not configured (coming in later update??)
### Stimulation Rate
Allows one to set the stimulation rate in a given sweep. Increasing the rate without altering the sweep settings will create a higher number of stimuli in a given sweep. Unlike stimulation levels, variations on this are created with a set number of increases (each by the step size), rather than increasing until a max condition is met. Varying this parameter can create what is known as a Repetition Rate Transfer Function, although of course one may attempt e.g. various ITDs at varying stimulation rates.
### ITD
Two operational modes here: list, and custom. Generally, creates delays between the two stimulation sides in order to emulate interaural time differences between the ears, which elicits special neuronal interactions in the brainstem and midbrain.
#### List
Uses the Start/Stop/Stepsize measures to create a list of ITD values to apply. Behaves similarly to stim level lists
#### Custom
Enter semi-colon separated list of custom ITD values, typically used for very long ITDs (greater than 10 ms or so) or ITDs not using linear steps
#### A Note on ITD Center
ITD stimuli are created by delaying the Right stimulus a static amount, and varying the amount of delay added to the Left stimulus to create an ITD. For example, delaying R by 15 and L by 17 creates an ITD of +2. Delaying R by 15 and L by 12 makes an ITD of -3. Itd Center reflects the static delay for the Right stimulus. However, this value summed together with the minimum (most negative) applied ITD must be greater than 0, or the program will throw an error. Otherwise, you would be attempting to create a negative delay, which is not possible with the current hardward configuration.


![Sweep Gui](sweepSettingsGui.png)
