function varargout = AcouElecStim(varargin)
% ACOUELECSTIM MATLAB code for AcouElecStim.fig
%      ACOUELECSTIM, by itself, creates a new ACOUELECSTIM or raises the existing
%      singleton*.
%
%      H = ACOUELECSTIM returns the handle to a new ACOUELECSTIM or the handle to
%      the existing singleton*.
%
%      ACOUELECSTIM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ACOUELECSTIM.M with the given input arguments.
%
%      ACOUELECSTIM('Property','Value',...) creates a new ACOUELECSTIM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AcouElecStim_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AcouElecStim_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

%% AcousticElectricStimulation - DOC 2020
% Created by Andrew Curran for the AG Vollmer in Magdeburg, with influence
% from Armin Wiegner and older stimulation GUIs
% Simple GUI for settings parameters in Brainware for delivery of acoustic
% and electric stimulation via DDE.

% Necessary External Functions:
% - settings_2014
% - loadRCO

% QUICK CHANGE GUIDE:
%   - The different types of stimulation, ITD, IO, RRTF, etc can be found
%   under 'presets' if you search
%   - For stimulus type (acoustic vs Electric) specific changes, the tag is 'stimModii'

%#ok<*INUSD>

% Last Modified by GUIDE v2.5 04-Apr-2019 12:48:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AcouElecStim_OpeningFcn, ...
                   'gui_OutputFcn',  @AcouElecStim_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AcouElecStim is made visible.
function AcouElecStim_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AcouElecStim (see VARARGIN)

% Choose default command line output for AcouElecStim
handles.output = hObject;

% Initialize important variables. Old version used global variable.
% Terrible idea, dont do it, use handles to pass variables around...

% Acoustic Calibration Files
handles.calibFileLeft = 'C:\projects\UKWcalibration\ear5_l.cal';
handles.calibFileRight = 'C:\projects\UKWcalibration\ear5_r.cal';
% Display calibrations for Acoustic stim
fd = fopen(handles.calibFileLeft,'r');
A = textscan(fd, '%s\t%s\t%s\t%s\t%d\t%d',1);
fclose(fd);
maxDb = A{5};
set(handles.dBsplLeft,'String',num2str(A{6}));
% Right side
fd = fopen(handles.calibFileRight,'r');
A = textscan(fd, '%s\t%s\t%s\t%s\t%d\t%d',1);
fclose(fd);
maxDb = min([maxDb A{5}]);
set(handles.maxDB,'String',num2str(maxDb));
set(handles.dBsplRight,'String',num2str(A{6}));

% Sweep Settings (Defaults)
handles.sweepLength = 400;
handles.repRate = 600;
handles.presOrder = 1;
handles.stimRepeats = 20;
handles.respStart = 10;
handles.respEnd = 300;
handles.sponStart = 390;
handles.sponEnd = 400;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AcouElecStim wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = AcouElecStim_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in sweepSettings.
function sweepSettings_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to sweepSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

h = settings_2018(handles);
uiwait(h);

s = load('settings_2018.mat', '-mat');
handles.sweepLength = s.handles.sweepLength;
handles.repRate = s.handles.repRate;
handles.presOrder = s.handles.presOrder;
handles.stimRepeats = s.handles.stimRepeats;
handles.respStart = s.handles.respStart;
handles.respEnd = s.handles.respEnd;
handles.sponStart = s.handles.sponStart;
handles.sponEnd = s.handles.sponEnd;
guidata(hObject, handles);

function setSettings(chan_Set)
handles = guidata(gcbo);
ddepoke(chan_Set, 'SweepLen',handles.sweepLength);
ddepoke(chan_Set, 'StimPeriod',handles.repRate);
ddepoke(chan_Set, 'PresOrder',handles.presOrder);
ddepoke(chan_Set, 'StimRepeats',handles.stimRepeats);
ddepoke(chan_Set, 'ResponseStart',handles.respStart);
ddepoke(chan_Set, 'ResponseEnd',handles.respEnd);
ddepoke(chan_Set, 'SponStart',handles.sponStart);
ddepoke(chan_Set, 'SponEnd',handles.sponEnd);

% --- Executes on button press in stimgridGen.
function handles = stimgridGen_Callback(hObject, eventdata, handles)
% hObject    handle to stimgridGen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% 2019.01.23 AC: Added L,R,S sweep additions partially. Always created for
% all cases of ITD

% This function calculates the stimgrid as used in BrainWare during the
% experiment. This function is separated from download for now.

% Helper anon func for legibility later; pulls numbers from handle strings
qkNum = @(x) str2double(get(handles.(x),'String')); % For typed in values
qkMenuVal = @(x) get(handles.(x),'Value'); %For buttons and the like

% Define passed attributes. Start with common parameters
commonHeader = {    'sweepDur',...
                    'stimWinDur',...
                    'itdCenter',...
                    'itd',...
                    'pulseCount',...
                    'delayLeft',...
                    'delayRight',...
                    'ipiLeft',...
                    'ipiRight'}; % data_L/R on this page but not included in list, end Train page
                    
% Get mux values for various stims
modiMux = qkMenuVal('stimModii');
acStim = qkMenuVal('acStim');
auralSel = qkMenuVal('auralSel');
assert(modiMux ~= 1 && acStim ~= 1 && auralSel ~= 1, 'Please make a selection for Stim Modii, Stim Type, and Mon/Binauralness!')
switch modiMux
    case 1 % Not yet selected
        error('Choose a modulus for stimulation first!')
%     case 2 %AC/AC
%         gridHeader = {  'dbSPL',...
%                         'freqLeft',...
%                         'freqRight',...
%                         'phaseRight',...
%                         'noiseCF',...
%                         'noiseQ',... %End stim gen page
%                         'mux',... % mux page
%                         'Atten1 [dB]',... % For PA5 dynamic attenuation control
%                         'Atten3 [dB]',...
%                         'ampLeft',...
%                         'ampRight'};
%     case 3 %EL/EL
%         gridHeader = {  'phaseDur',...
%                         'negPhaseDelay',...  % end Train page
%                         'attLeft',...
%                         'attRight',... % 
%                         'decimalAttLeft',...
%                         'decimalAttRight',... % end Output
%                         'startLeft',...
%                         'startRight'};
    case {2,3,4,5}%{4,5} %AC/EL
        gridHeader = {  'leftDelay',...
                        'phaseDur',...
                        'negPhaseDelay',... % data_L/R on this page but not included in list, end Train page
                        'ampLeft',...
                        'decimalAttRight',... % end Mux
                        'mux',...
                        'Atten1 [dB]',... % For PA5 dynamic attenuation control
                        'Atten3 [dB]',...
                        'attLeft',... % For ATT20 attenuation control
                        'attRight',...
                        'elecRightAttOffset',... % for pure electric stimulation
                        'dbSPL'};
end
gridHeader = [commonHeader gridHeader];
% Add empty row for data
gridHeader = [gridHeader; cell(1,length(gridHeader))];
% Useful function for adding cells
insertVal = @(y) strcmp(gridHeader(1,:),y);

%% Add static parameters. Rather than call specific table positions, locate
% based on string for robustness against future changes
stimWinDur = qkNum('stimDur');
assert(handles.sweepLength>stimWinDur,'Sweep duration must be larger than stimulus duration!')
gridHeader{2,insertVal('sweepDur')} = handles.sweepLength;
gridHeader{2,insertVal('stimWinDur')} = stimWinDur;

assert((qkNum('maxBbDiff')==0)|((qkNum('itdStepSize')==0)&(isempty(qkMenuVal('itdCustom')))),...
    'Only one itd funtion at a time! You have enabled some combination of binaural beat, short ITD, and/or long ITD!')
%% ITDs (itdCenter and itdList)
gridHeader = calcITD(handles,gridHeader);

% Timing (delayLeft/Right, ipiLeft/Right)
globalIpi = 1000/qkNum('ipi');
pulseCount = max([1 round(stimWinDur/globalIpi)]); % would use 0 if answer was zero, as for Long ITDs
bbDiff = qkNum('maxBbDiff');
globalDelay = qkNum('onsetDelay');
if bbDiff~=0 % Binaural Beat present
    % Similar to ITD; we fix right side and vary left side by altering the
    % static delay and IPI
    assert(globalIpi<stimWinDur+globalDelay,'You need at least two pulses to run Binaural Beats!')
    assert(pulseCount>1,'You need at least two pulses to run Binaural Beats!')
    delayLeft = globalDelay;
    delayRight = globalDelay + bbDiff; % Shifted later so that L goes first
    ipiRight = globalIpi;
    % The magic! Calculate a new IPI for left stim that causes an even
    % division of ITDs compared to Right
    ipiLeft = 2*bbDiff/(pulseCount-1)+ipiRight;
else
    % RRTF Calculations
    stimRateSteps = qkNum('stimRateStepCount');
    stimRateSize = qkNum('stimRateStepSize');
    
    if stimRateSteps > 0 && stimRateSize > 0
        ipi = 1000/globalIpi;
        repRate = ipi:stimRateSize:(ipi+stimRateSteps*stimRateSize);
        ipiLeft = 1000./repRate;
        ipiRight = 1000./repRate;
        pulseCount = max([ones(1,stimRateSteps+1); round(stimWinDur./ipiLeft)]);
    else
        ipiLeft = globalIpi;
        ipiRight = globalIpi;
    end
    delayLeft = globalDelay;
    delayRight = globalDelay;

end
% Insert to table
gridHeader{2,insertVal('delayLeft')} = delayLeft;
gridHeader{2,insertVal('delayRight')} = delayRight;
gridHeader{2,insertVal('ipiLeft')} = ipiLeft;
gridHeader{2,insertVal('ipiRight')} = ipiRight;
gridHeader{2,insertVal('pulseCount')} = pulseCount;

%% Stim specific Dynamic Parameters
% Attenuation
if any(modiMux == [2 4 5]) %require dbSPL attenuation
    assert(round(qkNum('maxDB'))==100,'For current program, max decible MUST be plus/minus 0.5 from 100!')
    acouMinLevel = qkNum('acouMinLevel');
    acouMaxLevel = qkNum('acouMaxLevel');
    acouStepSize = qkNum('acouStepSize');
    if acouStepSize == 0
        acouAttList = 100-acouMinLevel;
        dbSPL = acouMinLevel;
    else
        dbSPL = acouMinLevel:acouStepSize:acouMaxLevel;
        acouAttList = 100-dbSPL;
    end
    gridHeader{2,insertVal('dbSPL')} = dbSPL;
else % no db SPL, zero it out
    gridHeader{2,insertVal('dbSPL')} = 0;
end
if any(modiMux == [2 3 4 5]) % electric attenuation
    elecMinLevel = qkNum('elecMinLevel');
    elecMaxLevel = qkNum('elecMaxLevel');
    elecStepSize = qkNum('elecStepSize');
    elecRightAttOffset   = qkNum('elecOffset');
    if elecStepSize == 0
        elecAttList = elecMinLevel;
    else
        elecAttList = elecMinLevel:elecStepSize:elecMaxLevel;
    end
    attDecimal = elecAttList-floor(elecAttList);
    decimalAttLeft = db2mag(-attDecimal);
    decimalAttRight = decimalAttLeft;
end
% Stimulus Type for acoustic
if modiMux == 2 %only pure acoustic can have non-click/chirp stimulus
    switch acStim
        case 2
        case 3 % TC active
            gridHeader = calcFreq(handles, gridHeader,acouAttList,auralSel);
        otherwise
            error('Not yet Implemented')
    end
end
if any(modiMux == [2, 3, 4, 5]) % elec stim present
    phaseDur = qkNum('pulsePhaseDur')/1000;
    negPhaseDelay = phaseDur + qkNum('pulseIPhI')/1000;
end
if any(modiMux == [2,3,4,5]) % AC/EL stim. For now, assume click and pulse and ITD
    % Create click
    clickSigDur = qkNum('clickSigDur');
    click_sam = round(clickSigDur/10.24); %samples
    %dur_sam = round((dur*1000)/10.24); % samples
    
    data_L = zeros(1, click_sam+10);
    data_L(1, 2:(click_sam+1), 1) = -1;
    %data_R=data_L;
    handles.data_L=data_L;
    
    tempLeftDelay = qkNum('leftDelay');
    setStr = @(x,y) set(handles.(x), 'String', num2str(y));
    setStr('elecOffset',0);
    if modiMux == 4 % Code added to turn 'Elec Delay' into a 'Left Stimulus Delay'
        % Means Electric  Left, Acoustic Right. Flip the delay!
        gridHeader{2,insertVal('leftDelay')} = tempLeftDelay*(-1);
        leftDelay = tempLeftDelay*(-1);
    else
        gridHeader{2,insertVal('leftDelay')} = tempLeftDelay;
        leftDelay = tempLeftDelay;
    end
    
    % Some quick error checking before we really begin (added 17.04.2019)
    itdStart = min(gridHeader{2,strcmp({gridHeader{1,:}},'itd')});
    itdCenter = qkNum('itdCenter');
    
    if (leftDelay+itdStart+itdCenter)<0
        errordlg('Invalid combination of elec delay, ITD center, and Lowest ITD!','TIMING ERROR')
    end
    assert((leftDelay+itdStart+itdCenter)>0,'Invalid combination of elec delay, ITD center, and Lowest ITD!')
    
    gridHeader{2,insertVal('phaseDur')} = phaseDur;
    gridHeader{2,insertVal('negPhaseDelay')} = negPhaseDelay;
    gridHeader{2,insertVal('ampLeft')} = 1;
    gridHeader{2,insertVal('decimalAttRight')} = decimalAttRight;
    %% ACOU/ELEC SIG MUX!
    % Must be translated from GUI mux to RCO mux (different values to
    % pass!). 4 = ac Left, el Right and 5 vice versa. In RCO, mux 0 = el
    % Right, ac Left, and mux 1 is vice versa. At the moment, a simple
    % offset of 4 calculates this.
    %gridHeader{2,insertVal('mux')} = modiMux-4;
    %% Attenuation
    switch modiMux
        case 2 % AC/AC
            gridHeader{2,insertVal('Atten1 [dB]')} = 100-acouMinLevel;
            gridHeader{2,insertVal('Atten3 [dB]')} = 100-acouMinLevel;
            gridHeader{2,insertVal('attLeft')} = 120;
            gridHeader{2,insertVal('attRight')} = 120;
            gridHeader{2,insertVal('elecRightAttOffset')} = 0;
            gridHeader{2,insertVal('decimalAttRight')} = decimalAttRight;
            gridHeader{2,insertVal('mux')} = 2;
        case 3 % EL/EL
            gridHeader{2,insertVal('Atten1 [dB]')} = 120;
            gridHeader{2,insertVal('Atten3 [dB]')} = 120;
            gridHeader{2,insertVal('attLeft')} = elecMinLevel;
            gridHeader{2,insertVal('attRight')} = elecMinLevel+elecRightAttOffset;
            gridHeader{2,insertVal('elecRightAttOffset')} = elecRightAttOffset;
            gridHeader{2,insertVal('decimalAttRight')} = decimalAttRight;
            gridHeader{2,insertVal('mux')} = 3;
        case 4 % ac Left, el Right
            gridHeader{2,insertVal('Atten1 [dB]')} = 100-acouMinLevel;
            gridHeader{2,insertVal('Atten3 [dB]')} = 120;
            gridHeader{2,insertVal('attLeft')} = 120;
            gridHeader{2,insertVal('attRight')} = elecMinLevel;
            gridHeader{2,insertVal('elecRightAttOffset')} = elecRightAttOffset;
            gridHeader{2,insertVal('decimalAttRight')} = decimalAttRight;
            gridHeader{2,insertVal('mux')} = 0;
        case 5 % % ac Right, el Left
            gridHeader{2,insertVal('Atten1 [dB]')} = 120;
            gridHeader{2,insertVal('Atten3 [dB]')} = 100-acouMinLevel;
            gridHeader{2,insertVal('attLeft')} = elecMinLevel;
            gridHeader{2,insertVal('attRight')} = 120;
            gridHeader{2,insertVal('elecRightAttOffset')} = elecRightAttOffset;
            gridHeader{2,insertVal('decimalAttRight')} = decimalAttRight;
            gridHeader{2,insertVal('mux')} = 1;
    end    
    
else
    error('Not finished other implementations')
end

% Expand singleton dimensions to match longest.
finalLength = max(cell2mat(cellfun(@(x) length(x),gridHeader(2,:),'UniformOutput',0)));
for ii = 1:size(gridHeader,2)
    if length(gridHeader{2,ii})==1
        insertVect = ones(finalLength,1)*gridHeader{2,ii};
        gridHeader{2,ii} = insertVect;
    end
end
% and expand
data = cell(finalLength+1,size(gridHeader,2));
for ii = 1:size(gridHeader,2)
    data(1,ii) = gridHeader(1,ii);
    data(2:end,ii) = num2cell(gridHeader{2,ii});
end
% Ugh, quick fix for adding L, R, bds only runs
presets = qkMenuVal('presets');
if presets ~=5
if modiMux == 4
    lOnly = data(end,:);
    lOnly{4} = 0;
    lOnly{17} = 120;
    lOnly{18} = 120;
    lOnly{19} = 120;
    rOnly = data(end,:);
    rOnly{4} = 0;
    rOnly{16} = 120;
    rOnly{17} = 120;
    rOnly{18} = 120;
    sil = data(end,:);
    sil{4} = 0;
    sil{16} = 120;
    sil{17} = 120;
    sil{18} = 120;
    sil{19} = 120;
    sil{20} = 0;
    data = [data; lOnly; rOnly; sil];
    
end
if modiMux == 5
    lOnly = data(end,:);
    lOnly{4} = 0;
    lOnly{16} = 120;
    lOnly{18} = 120;
    lOnly{19} = 120;
    rOnly = data(end,:);
    rOnly{4} = 0;
    rOnly{16} = 120;
    rOnly{17} = 120;
    rOnly{19} = 120;
    sil = data(end,:);
    sil{4} = 0;
    sil{16} = 120;
    sil{17} = 120;
    sil{18} = 120;
    sil{19} = 120;
    sil{20} = 0;
    data = [data; lOnly; rOnly; sil];
    
end
if modiMux == 2
    lOnly = data(end,:);
    lOnly{4} = 0;
    lOnly{17} = 120;
    lOnly{18} = 120;
    lOnly{19} = 120;
    rOnly = data(end,:);
    rOnly{4} = 0;
    rOnly{16} = 120;
    rOnly{18} = 120;
    rOnly{19} = 120;
    sil = data(end,:);
    sil{4} = 0;
    sil{16} = 120;
    sil{17} = 120;
    sil{18} = 120;
    sil{19} = 120;
    sil{20} = 0;
    data = [data; lOnly; rOnly; sil];
    
end
if modiMux == 3
    lOnly = data(end,:);
    lOnly{4} = 0;
    lOnly{16} = 120;
    lOnly{17} = 120;
    lOnly{19} = 120;
    rOnly = data(end,:);
    rOnly{4} = 0;
    rOnly{16} = 120;
    rOnly{17} = 120;
    rOnly{18} = 120;
    sil = data(end,:);
    sil{4} = 0;
    sil{16} = 120;
    sil{17} = 120;
    sil{18} = 120;
    sil{19} = 120;
    sil{20} = 0;
    data = [data; lOnly; rOnly; sil];
    
end
end
data  %#ok<NOPRT>
handles.data=data;

function commonHeader = calcITD(handles, commonHeader)
% Helper function to simplify main function. Creates list of passed ITD
% values.
% 22.01.2019 AC: Made changes to allow non-ITD functionality; program now
% requires a value of 0 or empty value in Custom ITD values to function

% Helper anon func for legibility later; pulls numbers from handle strings
qkNum = @(x) str2double(get(handles.(x),'String')); % For typed in values
qkString = @(x) get(handles.(x),'String');

itdStart = qkNum('itdStart');
itdEnd = qkNum('itdEnd');
itdStepSize = qkNum('itdStepSize');
itdCustom = qkString('itdCustom');
itdCenter = qkNum('itdCenter');
customToks = strfind(itdCustom,';');
if ~isempty(itdCustom) % Use custom ITD string
    itdList = zeros(length(customToks)+1,1);
    if ~isempty(customToks)
        itdList(1) = str2double(itdCustom(1:customToks(1)-1)).*1000;
        for ii = 1:length(customToks)
            if ii == length(customToks)
                itdList(ii+1) = str2double(itdCustom(customToks(ii)+1:end)).*1000;
            else
                itdList(ii+1) = str2double(itdCustom(customToks(ii)+1:customToks(ii+1)-1)).*1000;
            end
        end
    else
        itdList(1) = str2double(itdCustom)*1000;
    end
elseif itdStart == 0 && itdStepSize == 0 && itdEnd == 0 % No itds present!
    itdList = 0;
else % Use normal ITDs
    itdList = (itdStart:itdStepSize:itdEnd)';
end

% Round to prevent sampling error and put into ms range
itdList = (round(itdList./10.24)*10.24)/1000;
% Add to table itd List and center ITD. This is more robust against
% shifting Brainware SG components
itdPos = strcmp(commonHeader(1,:),'itd');
itdCenPos = strcmp(commonHeader(1,:),'itdCenter');
commonHeader{2,itdPos} = itdList;
commonHeader{2,itdCenPos} = repmat(itdCenter,length(itdList),1);

function gridHeader = calcFreq(handles, gridHeader, acouAttList, auralSel)
% Helper function to simplify main function. Creates list of freq values
% permutated with attenuations
insertVal = @(y) strcmp(gridHeader(1,:),y);

attLength = length(acouAttList);
sineMinFreq = qkNum('sineMinFreq');
sineMaxFreq = qkNum('sineMaxFreq');
sineFreqPerOct = qkNum('sineFreqPerOct');
if sineFreqPerOct == 0
    freq = sineMinFreq;
else
    nOcts = log2(sineMaxFreq/sineMinFreq);
    nSteps = floor(nOcts*sineFreqPerOct);
    freq = round(sineMinFreq*2.^((0:nSteps)/nOcts));
end
% Pull calibration information
[calfrqL,calsplL] = readTDTCal(handles.calibFileLeft);
[calfrqR,calsplR, calphaseR] = readTDTCal(handles.calibFileRight);
% Correct missing phases
nanList = find(isnan(calphaseR));
for ii = 1:length(nanList)
    % If neighboring values available, take average between two
    if nanList(ii)-1 > 1 && nanList(ii)+1 < length(calphaseR)
        if ~isnan(calphaseR(nanList(ii)-1)) && ~isnan(calphaseR(nanList(ii)+1))
            calphaseR(nanList(ii)) = (calphaseR(nanList(ii)-1)+calphaseR(nanList(ii)+1))/2;
        else
            calphaseR(nanList(ii)) = 0;
        end
    else
        calphaseR(nanList(ii)) = 0;
    end
end
% Calibrate. This goes quickly, so do before setting attenuations. dbMax
% defines the db SPL output for a given frequency @ 100dbSPL click
% amplification (i.e. sometimes higher or lower than that.)
dbmaxL = interp1(calfrqL,calsplL,freq);
dbmaxR = interp1(calfrqR,calsplR,freq);
phase = interp1(calfrqR, calphaseR, freq);
nanList = isnan(dbmaxL);
for ii = 1:length(nanList)
    if freq(ii) <= calfrqL(1)
        dbmaxL(f) = calsplL(1);
        dbmaxR(f) = calsplR(1);
        phase(f) = calphaseR(1);
    else
        dbmaxL(f) = calsplL(length(calfrqL));
        dbmaxR(f) = calsplR(length(calfrqL));
        phase(f) = calphaseR(length(calfrqL));
    end
end

% Create list based on sided stimulus
if any(auralSel == [2 3 4]) % Left only stimulus present
        amplLeft = ones(size(freq, 2), attLength);
        % combine every possible freq and att value by repeating the
        % vectors across eachother and adding together for total
        % attenuation, corrected by 100 db SPL
        attLeft= repmat(dbmaxL',attLength)+repmat(acouAttList,size(dbmaxL'))-100;
        assert(~any(any(attLeft<-19.5)),'Some Frequencies for Left speaker are out of amplification range! Lower either the maximum frequency or maximum SPL level!')
        % if attenuation less than 0, amplify and set attenuation to 0
        idx = find(attLeft<0);
        ampls = db2mag(abs(attLeft(idx)));
        amplLeft(idx) = deal(ampls);
        attLeft(idx) = 0;
        % Reshape to gain ordered list of att/ampl/freq combinations. Freqs
        % are repeated for each possible db SPL and everything stored in a
        % vertical vector
        attLeft = reshape(attLeft', numel(attLeft),1);
        amplLeft = reshape(amplLeft', numel(amplLeft),1);
end
if any(auralSel == [2 3 5]) % Right only stimulus present
        amplRight = ones(size(freq, 2), attLength);
        % combine every possible freq and att value by repeating the
        % vectors across eachother and adding together for total
        % attenuation, corrected by 100 db SPL
        attRight= repmat(dbmaxL',attLength)+repmat(acouAttList,size(dbmaxL'))-100;
        assert(~any(any(attRight<-19.5)),'Some Frequencies for Right speaker are out of amplification range! Lower either the maximum frequency or maximum SPL level!')
        % if attenuation less than 0, amplify and set attenuation to 0
        idx = find(attRight<0);
        ampls = db2mag(abs(attRight(idx)));
        amplRight(idx) = deal(ampls);
        attRight(idx) = 0;
        % Reshape to gain ordered list of att/ampl/freq combinations. Freqs
        % are repeated for each possible db SPL and everything stored in a
        % vertical vector
        attRight = reshape(attRight', numel(attRight),1);
        amplRight = reshape(amplRight', numel(amplRight),1);
end
% Vectorize
% Make sure freq list is oriented correctly first!
freq = reshape(freq,length(freq),1);
% Lengthen freq vector to match single att list
freq = reshape(repmat(freq',attLength,1),numel(repmat(freq',attLength,1)),1);
phase = reshape(repmat(phase',attLength,1),numel(repmat(phase',attLength,1)),1);
switch auralSel
    case 2 % All three stimulus types. Order goes bds, L, R
        att1 = [attLeft;...
            attLeft;...
            ones(numel(attLeft),1)*120];
        att3 = [attRight;...
            ones(numel(attRight),1)*120;...
            attRight];
        ampl1 = [amplLeft;...
            amplLeft;...
            zeros(numel(amplLeft),1)];
        ampl3 = [amplRight;...
            zeros(numel(amplRight),1);...
            amplRight];
        freq = [freq; freq; freq];
        phase = [phase; phase; phase];
    case 3 % Left and Right Only. Order is L, R
        att1 = [attLeft;...
            ones(numel(attLeft),1)*120];
        att3 = [ones(numel(attRight),1)*120;...
            attRight];
        ampl1 = [amplLeft;...
            zeros(numel(amplLeft),1)];
        ampl3 = [zeros(numel(amplRight),1);...
            amplRight];
        freq = [freq; freq];
        phase = [phase; phase];
    case 4 % Left Only
        att1 = attLeft;
        att3 = ones(numel(attRight),1)*120;
        ampl1 = amplLeft;
        ampl3 = zeros(numel(amplRight),1);
        %freq = freq;
        %phase = phase;
    case 5 % Right Only
        att1 = ones(numel(attLeft),1)*120;
        att3 = attRight;
        ampl1 = zeros(numel(amplLeft),1);
        ampl3 = amplRight;
        %freq = freq;
        %phase = phase;
    case 6 % bds Only
        att1 = attLeft;
        att3 = attRight;
        ampl1 = amplLeft;
        ampl3 = amplRight;
        %freq = freq;
        %phase = phase;
end
% Add to table
gridHeader{2,insertVal('freqLeft')} = freq;
gridHeader{2,insertVal('freqRight')} = freq;
gridHeader{2,insertVal('phaseRight')} = phase;
gridHeader{2,insertVal('Atten1 [dB]')} = att1;
gridHeader{2,insertVal('Atten3 [dB]')} = att3;
gridHeader{2,insertVal('ampLeft')} = ampl1;
gridHeader{2,insertVal('ampRight')} = ampl3;
% enter placeholders for unused parameters
gridHeader{2,insertVal('noiseCF')} = 1;
gridHeader{2,insertVal('noiseQ')} = 1;

function offset = Att8Ch_Control(el_att_L, el_att_R)
% For electric signals the 8ch Attenuator is used. To control it
% two data streams have to be send on ByteA and ByteB
% of the TDT RX6: 1.value of attenuation  2.channel number

% round el_att values down. External attenuation on ATT20 is only cappable
% of integer values. The decimal part of set el_att is internally attenuated. 
el_att_L = floor(el_att_L);
el_att_R = floor(el_att_R);

rows = size(el_att_L, 1);
if size(el_att_L, 1) ~= size(el_att_L, 1)
    error('el_att_L and el_att_R must have the same length!')
end

% calculate channel numbers
UsedChannels = 8;   % if changing this value check "offset" values!
ChannelNumbers = 1:UsedChannels;
Ch = 2.^(ChannelNumbers-1);

% Set attenuation levels
% attenuation must not exceed 127dB!
% first row =127dB eg for aconly. 
max_db = 127;
Att = ones(rows+1, UsedChannels)*max_db;
for i=1:rows
    if el_att_L(i) > max_db
        el_att_L(i) = max_db;
    end
    if el_att_R(i) > max_db
        el_att_R(i) = max_db;
    end
    Att(i+1, 2) = el_att_L(i);
    Att(i+1, 3) = el_att_L(i);
    Att(i+1, 6) = el_att_R(i);
    Att(i+1, 7) = el_att_R(i);
end
%Att = [el_att_L, el_att_L, el_att_R, el_att_R, max_db, max_db, max_db, max_db];

% calculate timedata properties
block = 10;     % for 10samples (100µs) every attenuation stays stable
samples = round(block*(UsedChannels+1));
dur = samples*10.24/1000;
data_ch = zeros(1, samples);
data_att = zeros(1, samples);
data_att_temp = zeros(1, samples);
% calculate offset of counter to jump to next 'row' of accenuated rows
% offset = 0 is "off" = 127dB
offset = floor( (1:(rows))*samples/4 )';
% The '/4' in the formula is empirical and not understood. 

% calculate data
for n=ChannelNumbers-1  % for each attenuator channel
    data_ch(n*block+block/2:n*block+block/2+2) = Ch(n+1);
    data_att(n*block+2:n*block+block+1) = Att(1, n+1);
    data_att_plot = data_att;
end
for i=2:rows+1    % for each row in the stimulusgrid
    for n=ChannelNumbers-1  % for each attenuator channel
        data_att_temp(n*block+2:n*block+block+1) = Att(i, n+1);
    end
	data_att = [data_att, data_att_temp]; % accenuate next StimGridRow
	data_att_plot = [data_att_plot; data_att_temp];
end
size(data_ch);
size(data_att);
%size(data_att_plot)
%figure
%plot(data, [%data_att(0*samples+1:1*samples); 
            %data_att(1*samples+1:2*samples); 
            %data_att(2*samples+1:3*samples);
 %           data_att_plot; 
  %          data_ch])
%plot(data_att)

% load rco to RX6 - Attention that the rco's name is right!
RX6=actxcontrol('RPco.x',[1 1 1 1]);
if ~(RX6.ConnectRX6('GB',1))
    error('Unable to connect RX6');
end
handles.rco = '..\rco\8ch_Att_control';
RX6.ClearCOF;
if ~(RX6.LoadCOF(handles.rco))
    error('Cannot load RX6 circuit');
end

%UPLOAD static parameters to RX6
    RX6.WriteTagVEX('AttBuffer', 0, 'I8', data_att);
    RX6.WriteTagVEX('ChBuffer', 0, 'I8', data_ch);
    RX6.SetTagVal('AttWriteTime',dur);
    RX6.SetTagVal('Samples',samples);
    
if ~RX6.Run %Starts Circuit'
    error('Cannot run RX6 circuit');
end

% --- Executes on button press in download.
function download_Callback(hObject, eventdata, handles)
% hObject    handle to download (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% MAJOR BRAINWARE BUG!!!!!!
% 2018.12.17 AC: So it turns out that the reason older programs needed
% multiple resets when changing rco is due to the fact that BrainWare will
% erase the pre-existing settings on RX6 and RX8 upon reloading the
% stimgrid. The solution was to either call the functions again after
% loading the new stim grid, OR the chosen solution, which is to sim

% Helper anon func for legibility later; pulls numbers from handle strings
qkNum = @(x) str2double(get(handles.(x),'String')); % For typed in values
qkVal = @(x) get(handles.(x),'Value'); %For buttons and the like

% Save mat file for debugging later

saveFileName = [datestr(now,1) 'lastGenCall.mat'];
save(saveFileName)

chan= ddeinit('BrainWare32','StimulusTable');
assert(chan~=0,'Failed Brainware Connection!')
% check if correct stimconfig is loaded. Otherwise load it.
act_SC = ddereq(chan, 'StimConfig', [1,1]);
% Last character is carriage return or something else!
if ~strcmp(act_SC(1:end-1), 'C:\projects\LMU-Wue\s_c\AcElStim2018.s_c')
    if ~ddepoke(chan, 'StimConfig', 'C:\projects\LMU-Wue\s_c\AcElStim2018.s_c')
        error('Correct stimconfig (s_c) could not be loaded in Brainware!')
    end
end

% Signal Mux; Choosing a signal for each sides output
% First check for electric pulses, and simply set. Afterwards, we check the
% stim type for acoustic
modi = qkVal('stimModii');
% end
handles.rco = '..\rco\AcElStim2018';

assert(isfield(handles,'rco'),'Invalid RCO file!')

RX8=actxcontrol('RPco.x',[0 0 0 0]);
if(~invoke(RX8,'ConnectRX8','GB',1))
    error('Cannot open connect RX8');
end

invoke(RX8,'ClearCOF');
if(~invoke(RX8,'LoadCOF',handles.rco))
    error('Cannot load RX8 circuit');
end

% Run stimgrid generation first
handles = stimgridGen_Callback(hObject, eventdata, handles);

% One major change from previous programs is the usage of multiple rco
% files to keep run times low and unneccesary processing out of the window
% (e.g. we will not, in this program, perform tones and electric
% stimulation together, so why process sines in AC/EL stims?)
% Another change being attempted is composing just one signal to be sent to
% multiple destinations (why compose two biphasic pulses when one can be
% composed and copied?)

% Load 8chATT control first
data = handles.data;
attL = cell2mat(data(2:end,18));
attR = cell2mat(data(2:end,19));
offset = Att8Ch_Control(attL, attR);

offsetCell = [{'offset'}; num2cell(offset)];
data = [data offsetCell];

% chan= ddeinit('BrainWare32','StimulusTable');
% assert(chan~=0,'Failed Brainware Connection!')
% % check if correct stimconfig is loaded. Otherwise load it.
% act_SC = ddereq(chan, 'StimConfig', [1,1]);
% % Last character is carriage return or something else!
% if ~strcmp(act_SC(1:end-1), 'C:\projects\LMU-Wue\s_c\AcElStim2018.s_c')
%     if ~ddepoke(chan, 'StimConfig', 'C:\projects\LMU-Wue\s_c\AcElStim2018.s_c')
%         error('Correct stimconfig (s_c) could not be loaded in Brainware!')
%     end
% end

% Collect all titles of columns of stimgrid
paraname = [];
for i=1:size(data,2)
    ddepoke(chan, 'Column', i);
    paraname{i} = strtrim(ddereq(chan, 'ParameterName', [1, 1]));
    % Match our data grid to stimgrid
    idx(i) = find(strcmp(paraname(i),data(1,:)));
end
data = data(2:end,idx);
dlData = [];
for ii = 1:size(data,1)
    for jj = 1:size(data,2)
        dlData = [dlData sprintf('%d', data{ii, jj})]; %#ok<*AGROW>
        if jj <size(data,2)
            dlData = [dlData ','];
        end
    end
    dlData = [dlData char(10)];
end
ddepoke(chan, 'Editing','1');
if str2double(ddereq(chan, 'Editing', [1 1]))
    ddepoke(chan,'Contents', dlData);
    ddepoke(chan, 'Editing','0');
end
ddeterm(chan);
chan_Set= ddeinit('BrainWare32','Settings');
setSettings(chan_Set);
ddeterm(chan_Set);

% Upload click data
if isfield(handles,'data_L')
    RX8.WriteTagV('data_L',0,handles.data_L);
end
 
if(~invoke(RX8,'Run')) %Starts Circuit'
    error('Cannot run RX8 circuit');
end
Status = double(RX8.GetStatus); % Gets the status
% Checks for errors in starting circuit
if bitget(Status,1) == 0;
    disp('Error connecting to RP')
elseif bitget(Status,2) == 0; % Checks for connection
    disp('Error loading circuit')
elseif bitget(Status,3) == 0
    disp('error running circuit')
else
    disp('Circuit on RX8 is loaded and running')
end

% --- Executes on selection change in presets.
function presets_Callback(hObject, eventdata, handles)
% hObject    handle to presets (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns presets contents as cell array
%        contents{get(hObject,'Value')} returns selected item from presets
setStr = @(x,y) set(handles.(x), 'String', num2str(y));
setVal = @(x,y) set(handles.(x), 'Value', y);

sel = get(hObject,'Value');
switch sel
    case 3 % Short ITD
        % Acoustic Att
        %setStr('acouMinLevel',70);
        %setStr('acouMaxLevel',70);
        setStr('acouStepSize',0);
        % Elec Att
        %setStr('elecMinLevel',20);
        %setStr('elecMaxLevel',20);
        setStr('elecStepSize',0);
        setStr('elecOffset',0);
        % TC settings
        setStr('sineMinFreq',20);
        setStr('sineMaxFreq',0);
        setStr('sineFreqPerOct',0);
        % Stim rate
        setStr('ipi',20);
        setStr('stimRateStepCount',0);
        setStr('stimRateStepSize',0);
        % ITD settings
        setStr('itdStart',  -2000);
        setStr('itdEnd',     2000);
        setStr('itdStepSize',  50);
        setStr('itdCenter',10.0045);
        setStr('leftDelay',0);
        setStr('maxBbDiff',0);
        % Ensure custom ITDs are empty
        set(handles.itdCustom, 'String','');
        % Stim Configs
        setStr('clickSigDur', 50);
        setVal('clickChirpSet', 1);
        setStr('lowFreqFiltClick', 2000);
        setStr('highFreqFiltClick', 4000);
        setStr('noiseCenterFreq', 5000);
        setStr('noiseQ', 9600);
        setStr('pulsePhaseDur', 80);
        setStr('pulseIPhI', 10);
        % Sweep Settings on main Gui
        setStr('onsetDelay', 10);
        setStr('stimDur', 300);
        %setVal('stimModii', 4);
        setVal('acStim', 2);
        setVal('auralSel', 2);
        setVal('silSelFlag', 1);
        % Sweep Settings other Gui
        handles.sweepLength = 400;
        handles.repRate = 600;
        handles.presOrder = 1;
        handles.stimRepeats = 12;
        handles.respStart = 15;
        handles.respEnd = 50;
        handles.sponStart = 799;
        handles.sponEnd = 999;
        
        

    case 4 % Long ITD
        % Acoustic Att
        setStr('acouMinLevel',70);
        setStr('acouMaxLevel',70);
        setStr('acouStepSize',0);
        % Elec Att
        setStr('elecMinLevel',20);
        setStr('elecMaxLevel',20);
        setStr('elecStepSize',0);
        setStr('elecOffset',0);
        % TC settings
        setStr('sineMinFreq',20);
        setStr('sineMaxFreq',0);
        setStr('sineFreqPerOct',0);
        % Stim rate
        setStr('ipi',20);
        setStr('stimRateStepCount',0);
        setStr('stimRateStepSize',0);
        % ITD settings
        setStr('itdStart',  -0);
        setStr('itdEnd',     0);
        setStr('itdStepSize',0);
        setStr('itdCenter',214.999);
        setStr('leftDelay',0);
        setStr('maxBbDiff',0);
        % Ensure custom ITDs are empty
        set(handles.itdCustom, 'String',...
            '-200;-160;-120;-80;-60;-40;-20;-10;-5;-2;-1;0;1;2;5;10;20;40;60;80;120;160;200');
        % Stim Configs
        setStr('clickSigDur', 50);
        setVal('clickChirpSet', 1);
        setStr('lowFreqFiltClick', 2000);
        setStr('highFreqFiltClick', 4000);
        setStr('noiseCenterFreq', 5000);
        setStr('noiseQ', 9600);
        setStr('pulsePhaseDur', 80);
        setStr('pulseIPhI', 10);
        % Sweep Settings on main Gui
        setStr('onsetDelay', 10);
        setStr('stimDur', 10);
        %setVal('stimModii', 4);
        setVal('acStim', 2);
        setVal('auralSel', 2);
        setVal('silSelFlag', 1);
        % Sweep Settings other Gui
        handles.sweepLength = 500;
        handles.repRate = 900;
        handles.presOrder = 1;
        handles.stimRepeats = 20;
        handles.respStart = 5;
        handles.respEnd = 450;
        handles.sponStart = 799;
        handles.sponEnd = 999;
        
    case 5 %RRTF
        % Acoustic Att
        %setStr('acouMinLevel',70);
        %setStr('acouMaxLevel',70);
        setStr('acouStepSize',0);
        % Elec Att
        %setStr('elecMinLevel',20);
        %setStr('elecMaxLevel',20);
        setStr('elecStepSize',0);
        setStr('elecOffset',0);
        % TC settings
        setStr('sineMinFreq',20);
        setStr('sineMaxFreq',0);
        setStr('sineFreqPerOct',0);
        % Stim rate
        setStr('ipi',10);
        setStr('stimRateStepCount',39);
        setStr('stimRateStepSize',10);
        % ITD settings
        setStr('itdStart',      0);
        setStr('itdEnd',        0);
        setStr('itdStepSize',   0);
        setStr('itdCenter',10.0045);
        setStr('leftDelay',0);
        setStr('maxBbDiff',0);
        % Ensure custom ITDs are empty
        set(handles.itdCustom, 'String','0');
        % Stim Configs
        setStr('clickSigDur', 50);
        setVal('clickChirpSet', 1);
        setStr('lowFreqFiltClick', 2000);
        setStr('highFreqFiltClick', 4000);
        setStr('noiseCenterFreq', 5000);
        setStr('noiseQ', 9600);
        setStr('pulsePhaseDur', 80);
        setStr('pulseIPhI', 10);
        % Sweep Settings on main Gui
        setStr('onsetDelay', 10);
        setStr('stimDur', 501);
        %setVal('stimModii', 4);
        setVal('acStim', 2);
        setVal('auralSel', 2);
        setVal('silSelFlag', 1);
        % Sweep Settings other Gui
        handles.sweepLength = 600;
        handles.repRate = 1500;
        handles.presOrder = 2;
        handles.stimRepeats = 20;
        handles.respStart = 5;
        handles.respEnd = 575;
        handles.sponStart = 799;
        handles.sponEnd = 999;

end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function presets_CreateFcn(hObject, eventdata, handles)
% hObject    handle to presets (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in silSelFlag.
function silSelFlag_Callback(hObject, eventdata, handles)
% hObject    handle to silSelFlag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of silSelFlag



function onsetDelay_Callback(hObject, eventdata, handles)
% hObject    handle to onsetDelay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of onsetDelay as text
%        str2double(get(hObject,'String')) returns contents of onsetDelay as a double


% --- Executes during object creation, after setting all properties.
function onsetDelay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to onsetDelay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function stimDur_Callback(hObject, eventdata, handles)
% hObject    handle to stimDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of stimDur as text
%        str2double(get(hObject,'String')) returns contents of stimDur as a double


% --- Executes during object creation, after setting all properties.
function stimDur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stimDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in stimModii.
function stimModii_Callback(hObject, eventdata, handles)
% hObject    handle to stimModii (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns stimModii contents as cell array
%        contents{get(hObject,'Value')} returns selected item from stimModii
setStr = @(x,y) set(handles.(x), 'String', num2str(y));
setStr('elecOffset',0);
thisSet = get(hObject,'Value');
thisStr = get(hObject,'String');
if strcmp(thisStr(thisSet),'Acoustic/Electric')
    setStr('elecOrient','Electric = Right Stimulus');
elseif strcmp(thisStr(thisSet),'Electric/Acoustic')
    setStr('elecOrient','Electric = Left Stimulus');
elseif strcmp(thisStr(thisSet),'Electric/Electric')
    setStr('elecOrient','Delay should = 0!');
else
    setStr('elecOrient','Electric stim inactive');
end
setStr('leftDelay',0);


% --- Executes during object creation, after setting all properties.
function stimModii_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stimModii (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function stimRateStepSize_Callback(hObject, eventdata, handles)
% hObject    handle to stimRateStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of stimRateStepSize as text
%        str2double(get(hObject,'String')) returns contents of stimRateStepSize as a double


% --- Executes during object creation, after setting all properties.
function stimRateStepSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stimRateStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function stimRateStepCount_Callback(hObject, eventdata, handles)
% hObject    handle to stimRateStepCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of stimRateStepCount as text
%        str2double(get(hObject,'String')) returns contents of stimRateStepCount as a double


% --- Executes during object creation, after setting all properties.
function stimRateStepCount_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stimRateStepCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ipi_Callback(hObject, eventdata, handles)
% hObject    handle to ipi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ipi as text
%        str2double(get(hObject,'String')) returns contents of ipi as a double


% --- Executes during object creation, after setting all properties.
function ipi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ipi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sineMinFreq_Callback(hObject, eventdata, handles)
% hObject    handle to sineMinFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sineMinFreq as text
%        str2double(get(hObject,'String')) returns contents of sineMinFreq as a double


% --- Executes during object creation, after setting all properties.
function sineMinFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sineMinFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sineMaxFreq_Callback(hObject, eventdata, handles)
% hObject    handle to sineMaxFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sineMaxFreq as text
%        str2double(get(hObject,'String')) returns contents of sineMaxFreq as a double


% --- Executes during object creation, after setting all properties.
function sineMaxFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sineMaxFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sineFreqPerOct_Callback(hObject, eventdata, handles)
% hObject    handle to sineFreqPerOct (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sineFreqPerOct as text
%        str2double(get(hObject,'String')) returns contents of sineFreqPerOct as a double


% --- Executes during object creation, after setting all properties.
function sineFreqPerOct_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sineFreqPerOct (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function itdStart_Callback(hObject, eventdata, handles)
% hObject    handle to itdStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of itdStart as text
%        str2double(get(hObject,'String')) returns contents of itdStart as a double


% --- Executes during object creation, after setting all properties.
function itdStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to itdStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function itdEnd_Callback(hObject, eventdata, handles)
% hObject    handle to itdEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of itdEnd as text
%        str2double(get(hObject,'String')) returns contents of itdEnd as a double


% --- Executes during object creation, after setting all properties.
function itdEnd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to itdEnd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function itdStepSize_Callback(hObject, eventdata, handles)
% hObject    handle to itdStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of itdStepSize as text
%        str2double(get(hObject,'String')) returns contents of itdStepSize as a double


% --- Executes during object creation, after setting all properties.
function itdStepSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to itdStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function itdCustom_Callback(hObject, eventdata, handles)
% hObject    handle to itdCustom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of itdCustom as text
%        str2double(get(hObject,'String')) returns contents of itdCustom as a double


% --- Executes during object creation, after setting all properties.
function itdCustom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to itdCustom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function itdCenter_Callback(hObject, eventdata, handles)
% hObject    handle to itdCenter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of itdCenter as text
%        str2double(get(hObject,'String')) returns contents of itdCenter as a double
% Adding in error checking against invalid entries from elec delay


set(hObject,'String',num2str(round(1000*str2double(get(hObject,'String'))/10.24)*10.24/1000));


% --- Executes during object creation, after setting all properties.
function itdCenter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to itdCenter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function leftDelay_Callback(hObject, eventdata, handles)
% hObject    handle to leftDelay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% Hints: get(hObject,'String') returns contents of leftDelay as text
%        str2double(get(hObject,'String')) returns contents of leftDelay as a double


% --- Executes during object creation, after setting all properties.
function leftDelay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to leftDelay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pulsePhaseDur_Callback(hObject, eventdata, handles)
% hObject    handle to pulsePhaseDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pulsePhaseDur as text
%        str2double(get(hObject,'String')) returns contents of pulsePhaseDur as a double


% --- Executes during object creation, after setting all properties.
function pulsePhaseDur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pulsePhaseDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pulseIPhI_Callback(hObject, eventdata, handles)
% hObject    handle to pulseIPhI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pulseIPhI as text
%        str2double(get(hObject,'String')) returns contents of pulseIPhI as a double


% --- Executes during object creation, after setting all properties.
function pulseIPhI_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pulseIPhI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function noiseQ_Callback(hObject, eventdata, handles)
% hObject    handle to noiseQ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of noiseQ as text
%        str2double(get(hObject,'String')) returns contents of noiseQ as a double


% --- Executes during object creation, after setting all properties.
function noiseQ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noiseQ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function noiseCenterFreq_Callback(hObject, eventdata, handles)
% hObject    handle to noiseCenterFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of noiseCenterFreq as text
%        str2double(get(hObject,'String')) returns contents of noiseCenterFreq as a double


% --- Executes during object creation, after setting all properties.
function noiseCenterFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noiseCenterFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function clickSigDur_Callback(hObject, eventdata, handles)
% hObject    handle to clickSigDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of clickSigDur as text
%        str2double(get(hObject,'String')) returns contents of clickSigDur as a double


% --- Executes during object creation, after setting all properties.
function clickSigDur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to clickSigDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elecMinLevel_Callback(hObject, eventdata, handles)
% hObject    handle to elecMinLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elecMinLevel as text
%        str2double(get(hObject,'String')) returns contents of elecMinLevel as a double


% --- Executes during object creation, after setting all properties.
function elecMinLevel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elecMinLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elecMaxLevel_Callback(hObject, eventdata, handles)
% hObject    handle to elecMaxLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elecMaxLevel as text
%        str2double(get(hObject,'String')) returns contents of elecMaxLevel as a double


% --- Executes during object creation, after setting all properties.
function elecMaxLevel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elecMaxLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elecStepSize_Callback(hObject, eventdata, handles)
% hObject    handle to elecStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elecStepSize as text
%        str2double(get(hObject,'String')) returns contents of elecStepSize as a double


% --- Executes during object creation, after setting all properties.
function elecStepSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elecStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function acouStepSize_Callback(hObject, eventdata, handles)
% hObject    handle to acouStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of acouStepSize as text
%        str2double(get(hObject,'String')) returns contents of acouStepSize as a double


% --- Executes during object creation, after setting all properties.
function acouStepSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to acouStepSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function acouMaxLevel_Callback(hObject, eventdata, handles)
% hObject    handle to acouMaxLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of acouMaxLevel as text
%        str2double(get(hObject,'String')) returns contents of acouMaxLevel as a double


% --- Executes during object creation, after setting all properties.
function acouMaxLevel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to acouMaxLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function acouMinLevel_Callback(hObject, eventdata, handles)
% hObject    handle to acouMinLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of acouMinLevel as text
%        str2double(get(hObject,'String')) returns contents of acouMinLevel as a double


% --- Executes during object creation, after setting all properties.
function acouMinLevel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to acouMinLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in clickChirpSet.
function clickChirpSet_Callback(hObject, eventdata, handles)
% hObject    handle to clickChirpSet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns clickChirpSet contents as cell array
%        contents{get(hObject,'Value')} returns selected item from clickChirpSet


% --- Executes during object creation, after setting all properties.
function clickChirpSet_CreateFcn(hObject, eventdata, handles)
% hObject    handle to clickChirpSet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowFreqFiltClick_Callback(hObject, eventdata, handles)
% hObject    handle to lowFreqFiltClick (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lowFreqFiltClick as text
%        str2double(get(hObject,'String')) returns contents of lowFreqFiltClick as a double


% --- Executes during object creation, after setting all properties.
function lowFreqFiltClick_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lowFreqFiltClick (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function highFreqFiltClick_Callback(hObject, eventdata, handles)
% hObject    handle to highFreqFiltClick (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of highFreqFiltClick as text
%        str2double(get(hObject,'String')) returns contents of highFreqFiltClick as a double


% --- Executes during object creation, after setting all properties.
function highFreqFiltClick_CreateFcn(hObject, eventdata, handles)
% hObject    handle to highFreqFiltClick (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in maxBbDiff.
function maxBbDiff_Callback(hObject, eventdata, handles)
% hObject    handle to maxBbDiff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of maxBbDiff


% --- Executes on selection change in acStim.
function acStim_Callback(hObject, eventdata, handles)
% hObject    handle to acStim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns acStim contents as cell array
%        contents{get(hObject,'Value')} returns selected item from acStim


% --- Executes during object creation, after setting all properties.
function acStim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to acStim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in auralSel.
function auralSel_Callback(hObject, eventdata, handles)
% hObject    handle to auralSel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns auralSel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from auralSel


% --- Executes during object creation, after setting all properties.
function auralSel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to auralSel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function elecOffset_Callback(hObject, eventdata, handles)
% hObject    handle to elecOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of elecOffset as text
%        str2double(get(hObject,'String')) returns contents of elecOffset as a double


% --- Executes during object creation, after setting all properties.
function elecOffset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to elecOffset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
